# Front-End Task Installation Guide
 

## Getting Started:

Clone the repository then change the directory to `git clone https://gitlab.com/PerseveranceMbewe/front-end-fasttech.git `. Ounce you have cloned  the project using the follwing instructions on the section below. before you do that navigate to `front-end-5`.


### Installing Pacakges and Running the Project.

To run the application first install all the dependecies using `npm install` then   `npm run app` this will install all the packages on the client `client` side as well as on the `server` side. `Installing the packages may take some time please wait for all the node_modules to be installed`.

1. `npm install`
2. `npm run app `
3. `npm run dev`

The *server* should run on port `9090` and the client on port `3000`. Ounce the installation is done navigate to `http://localhost:3000`

These instuctions should take care of everything!!. 

