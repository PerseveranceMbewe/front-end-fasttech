import { useState, useEffect } from 'react';
import switchPages from './services/switchPages';
import ThemeProvider from './components/elements/ThemeProvider/ThemeProvider';
import axios from 'axios';
import MainPageContainer from './components/pages/MainPageContainer/MainPageContainer';

const App = () => {
  const [state, setState] = useState({
    currentStep: 1,
    progress: 11,
    userInfo: {
      firstName: '',
      lastName: '',
      nationality: '',
      country: '',
      gender: '',
      dob: '',
      contact: '',
      email: '',
      visited: false,
      relatives: false,
      howLong: 'Up To 3 months',
      why: [],
      industries: [],
      interests: [],
      saving: 'No savings',
      education: 'High school or less',
      educationTime: 'Right now'
    }
  });
  const [fetchedData, setFetchedData] = useState('');
  const BASE_URL = 'http://localhost:9090';

  useEffect(() => {
    const fetchData = async () => {
      const whyList = await axios.get(`${BASE_URL}/whyList`);
      const industries = await axios.get(`${BASE_URL}/industries`);
      const interests = await axios.get(`${BASE_URL}/interests`);
      const savings = await axios.get(`${BASE_URL}/savings`);
      const educationList = await axios.get(`${BASE_URL}/educationList`);
      const educationList2 = await axios.get(`${BASE_URL}/educationTimeList`);
      const howLongList = await axios.get(`${BASE_URL}/howLongList`);

      setFetchedData({
        whyList: whyList.data,
        industries: industries.data,
        interests: interests.data,
        savings: savings.data,
        educationList: educationList.data,
        educationList2: educationList2.data,
        howLongList: howLongList.data
      });
    };
    fetchData();
  }, []);

  return (
    <>
      <ThemeProvider>
        <MainPageContainer
          progress={state.progress}
          currentStep={state.currentStep}
        >
          {switchPages(state, setState, fetchedData)}
        </MainPageContainer>
      </ThemeProvider>
    </>
  );
};

export default App;