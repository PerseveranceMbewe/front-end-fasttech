import { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import Typography from '@material-ui/core/Typography/Typography';
import Grid from '@material-ui/core/Grid/Grid';
import Button from '../../elements/Button/Button';

const Step2 = ({ state, setState, howLongList }) => {
  const [visited, setVisited] = useState(state.userInfo.visited);
  const [relatives, setRelatives] = useState(state.userInfo.relatives);
  const [howLong, setHowLong] = useState(state.userInfo.howLong);

  const confirmStep = (e) => {
    setState({
      ...state,
      userInfo: {
        ...state.userInfo,
        visited,
        relatives,
        howLong
      },
      currentStep: state.currentStep + 1,
      progress: state.progress + 11
    });
  };

  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Typography variant='h5'>Have you ever visited dubai</Typography>
        </Grid>

        <Grid item xs={12}>
          <Button
            variant='extended'
            col={visited ? 'white' : 'pur'}
            onClick={() => setVisited(false)}
          >
            no
          </Button>
          <Button
            variant='extended'
            col={visited ? 'pur' : 'white'}
            onClick={() => setVisited(true)}
          >
            yes
          </Button>
        </Grid>

        <Grid item xs={12}>
          <Typography variant='h5'>
            Do you have any friends or family in Dubai?
          </Typography>
        </Grid>

        <Grid item xs={12}>
          <Button
            variant='extended'
            col={relatives ? 'white' : 'pur'}
            onClick={() => setRelatives(false)}
          >
            no
          </Button>
          <Button
            variant='extended'
            col={relatives ? 'pur' : 'white'}
            onClick={() => setRelatives(true)}
          >
            yes
          </Button>
        </Grid>

        <Grid item xs={12}>
          <Typography variant='h5'>
            How long do you want to stay in Dubai ?
          </Typography>
        </Grid>

        <Grid item xs={12}>
          {howLongList.map((long) => (
            <Button
              key={uuidv4()}
              variant='extended'
              col={long === howLong ? 'pur' : 'white'}
              onClick={() => setHowLong(long)}
            >
              {long}
            </Button>
          ))}
        </Grid>

        <Grid item xs={12}>
          <Button
            variant='extended'
            onClick={() =>
              setState({
                ...state,
                currentStep: state.currentStep - 1,
                progress: state.progress - 11
              })
            }
          >
            Back
          </Button>
          <Button
            col='pur'
            onClick={(e) => confirmStep(e)}
            variant='extended'
          >
            Next
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default Step2;
