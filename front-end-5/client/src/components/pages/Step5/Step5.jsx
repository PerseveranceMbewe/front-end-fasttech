import { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import Typography from '@material-ui/core/Typography/Typography';
import Grid from '@material-ui/core/Grid/Grid';
import Button from '../../elements/Button/Button';

const Step5 = ({ state, setState, availableInt }) => {
  const [interests, setInterests] = useState(state.userInfo.interests);
  const check = (item) => {
    return interests.includes(item);
  };

  const updateInt = (item) => {
    if (!check(item)) {
      const arr = [...interests, item];
      setInterests(arr);
    } else {
      const arr = interests.filter((int) => int !== item);
      setInterests(arr);
    }
  };

  const confirmStep = (e) => {
    ///////////////////////////
    //validators should be here
    ///////////////////////////
    setState({
      ...state,
      userInfo: {
        ...state.userInfo,
        interests: interests
      },
      currentStep: state.currentStep + 1,
      progress: state.progress + 11
    });
  };

  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Typography variant='h5'>What are your interests ?</Typography>
        </Grid>

        <Grid item xs={12}>
          {availableInt.map((int) => (
            <Button
              key={uuidv4()}
              variant='extended'
              col={check(int) ? 'pur' : 'white'}
              onClick={() => updateInt(int)}
            >
              {int}
            </Button>
          ))}
        </Grid>

        <Grid item xs={12}>
          <Button
            variant='extended'
            onClick={() =>
              setState({
                ...state,
                currentStep: state.currentStep - 1,
                progress: state.progress - 11
              })
            }
          >
            Back
          </Button>

          <Button
            col='pur'
            onClick={(e) => confirmStep(e)}
            variant='extended'
          >
            Next
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default Step5;
