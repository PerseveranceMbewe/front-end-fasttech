import { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import Typography from '@material-ui/core/Typography/Typography';
import Grid from '@material-ui/core/Grid/Grid';
import Button from '../../elements/Button/Button';

const Step8 = ({ state, setState, educationList }) => {
  const [education, setEducation] = useState(state.userInfo.educationTime);

  const confirmStep = (e) => {
    setState({
      ...state,
      userInfo: {
        ...state.userInfo,
        educationTime: education
      },
      currentStep: state.currentStep + 1,
      progress: state.progress + 12
    });
  };

  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Typography variant='h5'>
            What is your level of education ?
          </Typography>
        </Grid>

        <Grid item xs={12}>
          {educationList.map((educationItem) => (
            <Button
              key={uuidv4()}
              variant='extended'
              col={education === educationItem ? 'pur' : 'white'}
              onClick={() => setEducation(educationItem)}
            >
              {educationItem}
            </Button>
          ))}
        </Grid>

        <Grid item xs={12}>
          <Button
            variant='extended'
            onClick={() =>
              setState({
                ...state,
                currentStep: state.currentStep - 1,
                progress: state.progress - 11
              })
            }
          >
            Back
          </Button>
          <Button
            col='pur'
            onClick={(e) => confirmStep(e)}
            variant='extended'
          >
            Next
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default Step8;
